# How to Use

Let's say you have a REST service for returning a list of all
customers and their details.  The following examples use snippets
of JUnit 5, however, the pojo-table-assert library is agnostic of
testing frameworks.

You would add a factory to your test class:

```java
private PojoTableAsserterFactory factory;

@BeforeEach
void createFactory() {
    factory = new PojoTableAsserterFactory(Customer.class);
}
```

You can build an expected model in your tests like so:

```java
// @formatter:off
PojoTableAsserter asserter =
        factory.of    ("id", "name.firstName", "name.lastName")
               .addRow(101,  "John",           "Doe")
               .addRow(213,  "Jane",           "Smith")
               .addRow(215,  "James",          "Jones");
// @formatter:on
```

The `@formatter:off` and `@formatter:on` are common directives
understood by both Eclipse and Intellij that tell these IDEs to
not format the code between these lines.  We want this so that
the code remains as readable as possible.  We want our future
selves to look at this code and think to ourselves "Wow, that
looks like a real table!"

You can assert that everything is right using the following line:

```java
asserter.assertExpected(actualViewModels);
```

If this assert fails, it produces output that looks something
like this:

```
Expected: "
 id | name.firstName | name.lastName
101 | John           | Doe
213 | Jane           | Smith
215 | James          | Jones
"
but: was "
 id | name.firstName | name.lastName
101 | John           | Doe
213 | Jane           | Jane
215 | Jams           | Jones
"
```

And now you know you have a bug that causes the `firstName` to be
populated into the `lastName` field in certain circumstances (there's
also a typo in James' first name too!)

If you are using an IDE that understands this standard `assertEquals`
format, you will have access to the diff:

![Intellij's Comparison Failure Window](comparison-failure.png)