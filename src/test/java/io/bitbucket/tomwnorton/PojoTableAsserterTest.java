/*
 * pojo-table-assert
 * Copyright (C) 2018  Tom Norton
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.bitbucket.tomwnorton;

import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.jupiter.api.Assertions.*;

class PojoTableAsserterTest {
    private PojoTableAsserterFactory factory;

    @BeforeEach
    void createFactory() {
        factory = new PojoTableAsserterFactory(SimplePojo.class);
    }

    @Test
    void the_factor_cannot_be_created_with_a_null_pojo_class() {
        Throwable t = assertThrows(IllegalArgumentException.class, () -> new PojoTableAsserterFactory(null));
        assertThat(t.getMessage(), equalTo("Pojo class must not be null"));
    }

    @Test
    void it_cannot_be_created_with_a_null_array_of_property_paths() {
        Throwable t = assertThrows(IllegalArgumentException.class, () -> factory.of((String[]) null));
        assertThat(t.getMessage(), equalTo("Property paths must be provided"));
    }

    @Test
    void it_cannot_be_created_with_an_empty_array_of_property_paths() {
        Throwable t = assertThrows(IllegalArgumentException.class, factory::of);
        assertThat(t.getMessage(), equalTo("Property paths must be provided"));
    }

    @Test
    void it_cannot_be_created_with_a_null_property_path() {
        Throwable t = assertThrows(IllegalArgumentException.class, () -> factory.of((String) null));
        assertThat(t.getMessage(), equalTo("Property paths must not be null"));
    }

    @Test
    void it_cannot_be_created_with_invalid_property_paths() {
        Throwable t = assertThrows(IllegalArgumentException.class, () -> factory.of("foo", "bar", "baz"));
        assertThat(t.getMessage(), equalTo("baz is not a valid property of io.bitbucket.tomwnorton.SimplePojo"));
    }

    @Test
    void it_is_created_by_the_of_factory_method() {
        assertNotNull(factory.of("foo", "bar"));
    }

    @Nested
    class One_level_of_properties {
        private PojoTableAsserter asserter;

        @BeforeEach
        void createAsserter() {
            asserter = factory.of("foo", "bar");
        }

        @Test
        void it_cannot_add_a_null_expected_row() {
            Throwable t = assertThrows(IllegalArgumentException.class, () -> asserter.addRow((Object[]) null));
            assertThat(t.getMessage(), equalTo("Row must be provided"));
        }

        @Test
        void it_cannot_add_an_expected_row_whose_cell_count_is_different_from_the_number_of_property_paths() {
            Throwable t = assertThrows(IllegalArgumentException.class, () -> asserter.addRow(10, "hello", "goodbye"));
            assertThat(t.getMessage(), equalTo("Row must have exactly 2 cells"));
        }

        @Test
        void it_cannot_add_a_value_of_the_wrong_type_to_any_column() {
            Throwable t = assertThrows(IllegalArgumentException.class, () -> asserter.addRow("wrong", "hello"));
            assertThat(t.getMessage(), equalTo("Row 0 Column 0: Expected java.lang.Integer but got java.lang.String"));
        }

        @Test
        void it_allows_for_null_values() {
            assertDoesNotThrow(() -> asserter.addRow(10, null));
        }

        @Test
        void it_can_create_a_string_representation_of_an_empty_expected_table() {
            assertThat(asserter.formatExpectedTable(), equalTo("foo | bar"));
        }

        @Test
        void it_can_create_a_string_representation_of_an_expected_table_with_one_row() {
            asserter.addRow(256, "bit");

            // @formatter:off
            String expectedTable =
                    "foo | bar\n" +
                    "256 | bit";
            // @formatter:on

            assertThat(asserter.formatExpectedTable(), equalTo(expectedTable));
        }

        @Test
        void it_pads_the_data_columns_so_that_they_are_not_smaller_than_the_heading_columns() {
            asserter.addRow(4, "hi");

            // @formatter:off
            String expectedTable =
                    "foo | bar\n" +
                    "  4 | hi ";
            // @formatter:on

            assertThat(asserter.formatExpectedTable(), equalTo(expectedTable));
        }

        @Test
        void it_pads_the_heading_columns_so_that_they_are_not_smaller_than_the_data_columns() {
            asserter.addRow(16386, "Igor Stravinsky");

            // @formatter:off
            String expectedTable =
                    "  foo | bar            \n" +
                    "16386 | Igor Stravinsky";
            // @formatter:on

            assertThat(asserter.formatExpectedTable(), equalTo(expectedTable));
        }

        @Test
        void it_displays_null_values_correctly() {
            asserter.addRow(16386, null);

            // @formatter:off
            String expectedTable =
                    "  foo | bar \n" +
                    "16386 | null";
            // @formatter:on

            assertThat(asserter.formatExpectedTable(), equalTo(expectedTable));
        }

        @Test
        void it_can_create_a_string_that_represents_a_list_of_a_single_pojo() {
            SimplePojo simplePojo = new SimplePojo();
            simplePojo.setFoo(256);
            simplePojo.setBar("bit");

            // @formatter:off
            String expectedTable =
                    "foo | bar\n" +
                    "256 | bit";
            // @formatter:on

            assertThat(asserter.formatPojos(Collections.singletonList(simplePojo)), equalTo(expectedTable));
        }

        @Test
        void it_can_assert_that_the_pojos_are_equal_to_the_expected() {
            SimplePojo simplePojo = new SimplePojo();
            simplePojo.setFoo(256);
            simplePojo.setBar("bit");

            asserter.addRow(128, "bits");

            String expectedMessage = "\nExpected: \"\n" +
                    "foo | bar \n" +
                    "128 | bits\n" +
                    "\"\n" +
                    "     but: was \"\n" +
                    "foo | bar \n" +
                    "256 | bit \n" +
                    "\"";
            Throwable t = assertThrows(AssertionError.class,
                                       () -> asserter.assertExpected(Collections.singletonList(simplePojo)));
            assertThat(t.getMessage(), equalTo(expectedMessage));
        }
    }

    @Nested
    class Nested_levels_of_properties {
        @BeforeEach
        void setup() {
            factory = new PojoTableAsserterFactory(NestedPojo.class);
        }

        @Test
        void it_sees_the_nested_properties_has_valid() {
            assertDoesNotThrow(() -> factory.of("foobar.foo", "foobar.bar", "baz"));
        }

        @Test
        void it_can_create_a_string_representation_of_a_list_of_nested_pojos() {
            List<NestedPojo> pojos = new ArrayList<>();
            NestedPojo pojo = new NestedPojo();
            pojo.setFoobar(new SimplePojo());
            pojo.getFoobar().setFoo(10);
            pojo.getFoobar().setBar("hello");
            pojo.setBaz("world");
            pojos.add(pojo);

            pojo = new NestedPojo();
            pojo.setFoobar(new SimplePojo());
            pojo.getFoobar().setFoo(1024);
            pojo.getFoobar().setBar("bit");
            pojo.setBaz("kilobyte");
            pojos.add(pojo);

            // @formatter:off
            String expected =
                    "foobar.foo | foobar.bar | baz     \n" +
                    "        10 | hello      | world   \n" +
                    "      1024 | bit        | kilobyte";
            // @formatter:on

            PojoTableAsserter asserter = factory.of("foobar.foo", "foobar.bar", "baz");

            assertThat(asserter.formatPojos(pojos), equalTo(expected));
        }

        @Test
        void it_can_create_a_string_representation_of_a_nested_pojo_that_is_null() {
            List<NestedPojo> pojos = new ArrayList<>();
            NestedPojo pojo = new NestedPojo();
            pojo.setFoobar(null);
            pojo.setBaz("world");
            pojos.add(pojo);

            pojo = new NestedPojo();
            pojo.setFoobar(new SimplePojo());
            pojo.getFoobar().setFoo(1024);
            pojo.getFoobar().setBar("bit");
            pojo.setBaz("kilobyte");
            pojos.add(pojo);

            // @formatter:off
            String expected =
                    "foobar.foo | foobar.bar | baz     \n" +
                    "      null | null       | world   \n" +
                    "      1024 | bit        | kilobyte";
            // @formatter:on

            PojoTableAsserter asserter = factory.of("foobar.foo", "foobar.bar", "baz");

            assertThat(asserter.formatPojos(pojos), equalTo(expected));
        }
    }

    @Nested
    class It_formats_numbers {
        private NumbersPojo pojo;

        @BeforeEach
        void createFactory() {
            factory = new PojoTableAsserterFactory(NumbersPojo.class);
        }

        @BeforeEach
        void createPojo() {
            pojo = new NumbersPojo();
        }

        @Test
        void it_works_on_primitive_bytes() {
            byte value = 64;
            pojo.setPrimitiveByte(value);
            assertProperty("primitiveByte", value);
        }

        @Test
        void it_works_on_wrapper_bytes() {
            byte value = 64;
            pojo.setWrapperByte(value);
            assertProperty("wrapperByte", value);
        }

        @Test
        void it_works_on_primitive_shorts() {
            short value = 256;
            pojo.setPrimitiveShort(value);
            assertProperty("primitiveShort", value);
        }

        @Test
        void it_works_on_wrapper_shorts() {
            short value = 256;
            pojo.setWrapperShort(value);
            assertProperty("wrapperShort", value);
        }

        @Test
        void it_works_on_primitive_ints() {
            int value = 256;
            pojo.setPrimitiveInt(value);
            assertProperty("primitiveInt", value);
        }

        @Test
        void it_works_on_wrapper_integers() {
            int value = 256;
            pojo.setWrapperInteger(value);
            assertProperty("wrapperInteger", value);
        }

        @Test
        void it_works_on_primitive_longs() {
            long value = 256L;
            pojo.setPrimitiveLong(value);
            assertProperty("primitiveLong", value);
        }

        @Test
        void it_works_on_wrapper_longs() {
            long value = 256L;
            pojo.setWrapperLong(value);
            assertProperty("wrapperLong", value);
        }

        @Test
        void it_works_on_primitive_float() {
            float value = 123.75F;
            pojo.setPrimitiveFloat(value);
            assertProperty("primitiveFloat", value);
        }

        @Test
        void it_works_on_wrapper_float() {
            float value = 123.75F;
            pojo.setWrapperFloat(value);
            assertProperty("wrapperFloat", value);
        }

        @Test
        void it_works_on_primitive_double() {
            double value = 123.75;
            pojo.setPrimitiveDouble(value);
            assertProperty("primitiveDouble", value);
        }

        @Test
        void it_works_on_wrapper_double() {
            double value = 123.75;
            pojo.setWrapperDouble(value);
            assertProperty("wrapperDouble", value);
        }

        @Test
        void it_works_on_big_decimal() {
            BigDecimal value = BigDecimal.valueOf(100);
            pojo.setBigDecimal(value);
            assertProperty("bigDecimal", value);
        }

        private void assertProperty(String property, Object object) {
            String expectedValue = String.valueOf(object);
            int maxLength = Math.max(property.length(), expectedValue.length());
            String expectedTable = StringUtils.leftPad(property, maxLength) + "\n" +
                    StringUtils.leftPad(expectedValue, maxLength);
            PojoTableAsserter asserter = factory.of(property);
            assertThat(asserter.formatPojos(Collections.singletonList(pojo)), equalTo(expectedTable));
        }
    }

    @Nested
    class It_formats_booleans {
        private BooleansPojo pojo;

        @BeforeEach
        void createFactory() {
            factory = new PojoTableAsserterFactory(BooleansPojo.class);
        }

        @BeforeEach
        void createPojo() {
            pojo = new BooleansPojo();
        }

        @Test
        void it_works_on_primitive_booleans() {
            pojo.setPrimitiveBoolean(true);
            assertProperty("primitiveBoolean", true);
        }

        @Test
        void it_works_on_wrapper_booleans() {
            pojo.setWrapperBoolean(false);
            assertProperty("wrapperBoolean", false);
        }

        private void assertProperty(String property, Object object) {
            String expectedValue = String.valueOf(object);
            int maxLength = Math.max(property.length(), expectedValue.length());
            String expectedTable = StringUtils.rightPad(property, maxLength) + "\n" +
                    StringUtils.rightPad(expectedValue, maxLength);
            PojoTableAsserter asserter = factory.of(property);
            assertThat(asserter.formatPojos(Collections.singletonList(pojo)), equalTo(expectedTable));
        }
    }

    @Test
    void it_can_format_a_custom_class_using_its_to_string() {
        factory = new PojoTableAsserterFactory(NestedPojo.class);
        PojoTableAsserter asserter = factory.of("foobar", "baz");

        NestedPojo nestedPojo = new NestedPojo();
        nestedPojo.setFoobar(new SimplePojo());
        nestedPojo.getFoobar().setFoo(10);
        nestedPojo.getFoobar().setBar("ten");
        nestedPojo.setBaz("zero");

        // @formatter:off
        String expectedTable =
                "foobar | baz \n" +
                "10;ten | zero";
        // @formatter:on

        assertThat(asserter.formatPojos(Collections.singletonList(nestedPojo)), equalTo(expectedTable));
    }

    @Test
    void it_can_format_an_object_using_a_custom_formatter() {
        PropertyTypeFormatter propertyTypeFormatter = new PropertyTypeFormatter() {
            @Override
            public Alignment getAlignment() {
                return Alignment.LEFT;
            }

            @Override
            public String toString(Object value) {
                SimplePojo simplePojo = (SimplePojo) value;
                return simplePojo.getBar() + "-" + simplePojo.getFoo();
            }

            @Override
            public boolean isMatchingClass(Class<?> propertyType) {
                return SimplePojo.class.isAssignableFrom(propertyType);
            }
        };

        factory = new PojoTableAsserterFactory(NestedPojo.class, propertyTypeFormatter);
        PojoTableAsserter asserter = factory.of("foobar", "baz");

        NestedPojo nestedPojo = new NestedPojo();
        nestedPojo.setFoobar(new SimplePojo());
        nestedPojo.getFoobar().setFoo(10);
        nestedPojo.getFoobar().setBar("ten");
        nestedPojo.setBaz("zero");

        // @formatter:off
        String expectedTable =
                "foobar | baz \n" +
                "ten-10 | zero";
        // @formatter:on

        assertThat(asserter.formatPojos(Collections.singletonList(nestedPojo)), equalTo(expectedTable));
    }

    @Test
    void it_can_be_executed_using_a_fluent_api() {
        List<NestedPojo> pojos = new ArrayList<>();
        NestedPojo pojo = new NestedPojo();
        pojo.setFoobar(new SimplePojo());
        pojo.getFoobar().setFoo(10);
        pojo.getFoobar().setBar("hello");
        pojo.setBaz("world");
        pojos.add(pojo);

        pojo = new NestedPojo();
        pojo.setFoobar(new SimplePojo());
        pojo.getFoobar().setFoo(1024);
        pojo.getFoobar().setBar("bit");
        pojo.setBaz("kilobyte");
        pojos.add(pojo);

        // @formatter:off
            String expected =
                    "foobar.foo | foobar.bar | baz     \n" +
                    "        10 | hello      | world   \n" +
                    "      1024 | bit        | kilobyte";
        // @formatter:on

        // @formatter:off
        factory = new PojoTableAsserterFactory(NestedPojo.class);
        PojoTableAsserter asserter = factory
                .of    ("foobar.foo", "foobar.bar", "baz")
                .addRow(10,           "hello",      "world")
                .addRow(1024,         "bit",        "kilobyte");
        // @formatter:on

        assertThat(asserter.formatExpectedTable(), equalTo(expected));
    }
}