/*
 * pojo-table-assert
 * Copyright (C) 2018  Tom Norton
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.bitbucket.tomwnorton;

public class NestedPojo {
    private SimplePojo foobar;
    private String baz;

    public SimplePojo getFoobar() {
        return foobar;
    }

    public void setFoobar(SimplePojo foobar) {
        this.foobar = foobar;
    }

    public String getBaz() {
        return baz;
    }

    public void setBaz(String baz) {
        this.baz = baz;
    }
}
