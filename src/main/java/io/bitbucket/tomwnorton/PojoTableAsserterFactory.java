/*
 * pojo-table-assert
 * Copyright (C) 2018  Tom Norton
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.bitbucket.tomwnorton;

import org.apache.commons.lang3.ArrayUtils;

import java.util.*;

/**
 * Allows you to create multiple {@code PojoTableAsserter} instances while specifying the rules for it only once.
 */
public class PojoTableAsserterFactory {
    private final Class<?> pojoClass;
    private final List<PropertyTypeFormatter> propertyTypeFormatters;

    public PojoTableAsserterFactory(Class<?> pojoClass, PropertyTypeFormatter... propertyTypeFormatters) {
        guardAgainstNullConstructorArgs(pojoClass);
        this.propertyTypeFormatters = new ArrayList<>();
        this.propertyTypeFormatters.addAll(Arrays.asList(propertyTypeFormatters));
        this.propertyTypeFormatters.add(PropertyTypeFormatter.NUMERIC);
        this.propertyTypeFormatters.add(PropertyTypeFormatter.STRING);
        this.pojoClass = pojoClass;
    }

    private void guardAgainstNullConstructorArgs(Class<?> pojoClass) {
        if (pojoClass == null) {
            throw new IllegalArgumentException("Pojo class must not be null");
        }
    }

    public PojoTableAsserter of(String... propertyPaths) {
        guardAgainstNullPropertyPaths(propertyPaths);
        return new PojoTableAsserter(Arrays.asList(propertyPaths), pojoClass, propertyTypeFormatters);
    }

    private void guardAgainstNullPropertyPaths(String[] propertyPaths) {
        if (ArrayUtils.isEmpty(propertyPaths)) {
            throw new IllegalArgumentException("Property paths must be provided");
        }
        if (Arrays.stream(propertyPaths).anyMatch(Objects::isNull)) {
            throw new IllegalArgumentException("Property paths must not be null");
        }
    }
}
