/*
 * pojo-table-assert
 * Copyright (C) 2018  Tom Norton
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.bitbucket.tomwnorton;

/**
 * Allows you to customize how table cells for various types are formatted
 * <p>
 * You can customize table cells in two ways:
 * <ul>
 *     <li>Determine whether the table cell for these types should be left-aligned or right-aligned</li>
 *     <li>Determine how each cell is formatted.</li>
 * </ul>
 */
public interface PropertyTypeFormatter {
    enum Alignment {
        LEFT("-"),
        RIGHT("");

        final String alignmentPattern;

        Alignment(String alignmentPattern) {
            this.alignmentPattern = alignmentPattern;
        }
    }

    PropertyTypeFormatter NUMERIC = new PropertyTypeFormatter() {
        @Override
        public Alignment getAlignment() {
            return Alignment.RIGHT;
        }

        @Override
        public boolean isMatchingClass(Class<?> propertyType) {
            return Number.class.isAssignableFrom(propertyType);
        }
    };

    PropertyTypeFormatter STRING = new PropertyTypeFormatter() {
        @Override
        public Alignment getAlignment() {
            return Alignment.LEFT;
        }

        @Override
        public boolean isMatchingClass(Class<?> propertyType) {
            return true;
        }
    };

    default Alignment getAlignment() {
        return Alignment.LEFT;
    }

    default String toString(Object value) {
        return String.valueOf(value);
    }

    boolean isMatchingClass(Class<?> propertyType);
}
