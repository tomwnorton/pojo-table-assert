/*
 * pojo-table-assert
 * Copyright (C) 2018  Tom Norton
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.bitbucket.tomwnorton;

import org.apache.commons.lang3.StringUtils;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.util.*;
import java.util.function.Supplier;
import java.util.stream.Collectors;

/**
 * Allows you to assert that a list of POJOs is correct.  When the assert fails, then it will display both the expected values
 * and the actual values in human-readable tables.
 */
public class PojoTableAsserter {
    private final List<Column> columns;
    private final List<List<String>> expectedRows = new ArrayList<>();

    PojoTableAsserter(List<String> propertyPaths,
                      Class<?> pojoClass,
                      List<PropertyTypeFormatter> propertyTypeFormatters) {
        try {
            List<Column> columns = new ArrayList<>();
            PropertyDescriptor[] propertyDescriptors = Introspector.getBeanInfo(pojoClass).getPropertyDescriptors();
            for (String propertyPath : propertyPaths) {
                GetterChain getterChain = new GetterChain(pojoClass,
                                                          propertyPath,
                                                          () -> createInvalidPropertyException(propertyPath, pojoClass));
                columns.add(new Column(propertyPath, getterChain, propertyTypeFormatters));
            }
            this.columns = Collections.unmodifiableList(columns);
        } catch (IntrospectionException e) {
            throw new RuntimeException(e);
        }
    }

    private RuntimeException createInvalidPropertyException(String propertyPath, Class<?> pojoClass) {
        String pattern = "%s is not a valid property of %s";
        return new IllegalArgumentException(String.format(pattern, propertyPath, pojoClass.getName()));
    }

    public PojoTableAsserter addRow(Object... row) {
        guardAgainstBadRow(row);
        List<String> stringifiedRow = new ArrayList<>();
        for (int i = 0; i < row.length; i++) {
            stringifiedRow.add(columns.get(i).formatValue(row[i], expectedRows.size(), i));
        }
        expectedRows.add(stringifiedRow);
        return this;
    }

    private void guardAgainstBadRow(Object[] row) {
        if (row == null) {
            throw new IllegalArgumentException("Row must be provided");
        }
        if (row.length != columns.size()) {
            throw new IllegalArgumentException(String.format("Row must have exactly %d cells", columns.size()));
        }
    }

    /**
     * Throws an {@code AssertionError} if the pojos are not correct
     * <p>
     * The format of the {@code AssertionError} message ensures two things:
     * <ul>
     *     <li>The results are readable from the command line (e.g., maven)</li>
     *     <li>The results are readable from Intellij's diff view for {@code assertEquals} failures</li>
     * </ul>
     */
    public void assertExpected(List<?> pojos) {
        String expectedTable = formatExpectedTable();
        String actualTable = formatPojos(pojos);
        if (!expectedTable.equals(actualTable)) {
            String message =
                    "\nExpected: \"\n" + expectedTable + "\n\"" +
                            "\n     but: was \"\n" + actualTable + "\n\"";
            throw new AssertionError(message);
        }
    }

    String formatExpectedTable() {
        return getFormattedTable(expectedRows);
    }

    private String getFormattedTable(List<List<String>> rows) {
        List<String> formattedRows = new ArrayList<>();
        String pattern = columns.stream().map(Column::getFormatPattern).collect(Collectors.joining(" | "));
        formattedRows.add(String.format(pattern, columns.stream().map(column -> column.propertyPath).toArray()));
        rows.stream().map(row -> String.format(pattern, row.toArray())).forEach(formattedRows::add);
        return StringUtils.join(formattedRows, "\n");
    }

    String formatPojos(Iterable<?> pojos) {
        List<List<String>> rows = new ArrayList<>();
        for (Object pojo : pojos) {
            List<String> row = new ArrayList<>();
            for (Column column : columns) {
                row.add(column.getValueFromPojo(pojo, rows.size(), 0));
            }
            rows.add(row);
        }
        return getFormattedTable(rows);
    }

    private static class Column {
        private static final Map<Class<?>, Class<?>> PRIMITIVE_CLASSES_TO_WRAPPER_CLASSES_MAP;

        static {
            Map<Class<?>, Class<?>> temp = new HashMap<>();
            temp.put(byte.class, Byte.class);
            temp.put(short.class, Short.class);
            temp.put(int.class, Integer.class);
            temp.put(long.class, Long.class);
            temp.put(float.class, Float.class);
            temp.put(double.class, Double.class);
            temp.put(boolean.class, Boolean.class);
            PRIMITIVE_CLASSES_TO_WRAPPER_CLASSES_MAP = Collections.unmodifiableMap(temp);
        }

        private final String propertyPath;
        private final Class<?> propertyPathClass;
        private final PropertyTypeFormatter propertyTypeFormatter;
        private final GetterChain getterChain;
        private int maxLength;

        Column(String propertyPath,
               GetterChain getterChain,
               List<PropertyTypeFormatter> propertyTypeFormatters) {
            this.propertyPath = propertyPath;
            this.getterChain = getterChain;
            this.propertyPathClass = getWrapperClassIfApplicable(getterChain.getType());
            this.propertyTypeFormatter = propertyTypeFormatters.stream()
                    .filter(formatter -> formatter.isMatchingClass(this.propertyPathClass))
                    .findFirst()
                    .orElseThrow(() -> new IllegalStateException("BUG: Could not find way to format a type!"));
            maxLength = propertyPath.length();
        }

        private Class<?> getWrapperClassIfApplicable(Class<?> propertyPathClass) {
            if (PRIMITIVE_CLASSES_TO_WRAPPER_CLASSES_MAP.containsKey(propertyPathClass)) {
                return PRIMITIVE_CLASSES_TO_WRAPPER_CLASSES_MAP.get(propertyPathClass);
            }
            return propertyPathClass;
        }

        String formatValue(Object cellValue, int rowIndex, int cellIndex) {
            guardAgainstBadDataCell(cellValue, rowIndex, cellIndex);
            String value = propertyTypeFormatter.toString(cellValue);
            updateMaxLength(value.length());
            return value;
        }

        void guardAgainstBadDataCell(Object cellValue, int rowIndex, int cellIndex) {
            if (cellValue != null && !cellValue.getClass().equals(propertyPathClass)) {
                throw new IllegalArgumentException(String.format(
                        "Row %d Column %d: Expected %s but got %s",
                        rowIndex,
                        cellIndex,
                        propertyPathClass.getName(),
                        cellValue.getClass().getName()));
            }
        }

        void updateMaxLength(int cellLength) {
            if (cellLength > maxLength) {
                maxLength = cellLength;
            }
        }

        String getFormatPattern() {
            String alignment = propertyTypeFormatter.getAlignment().alignmentPattern;
            return "%" + alignment + maxLength + "s";
        }

        String getValueFromPojo(Object pojo, int rowIndex, int columnIndex) {
            return formatValue(getterChain.getValue(pojo), rowIndex, columnIndex);
        }
    }

    private class GetterChain {
        private List<Method> getterMethods = new ArrayList<>();
        private Class<?> type;

        GetterChain(Class<?> parentClass, String propertyPath, Supplier<? extends RuntimeException> exceptionSupplier) {
            String[] propertyParts = propertyPath.split("\\.");
            try {
                Class<?> currentClass = parentClass;
                for (String propertyPart : propertyParts) {
                    BeanInfo beanInfo = Introspector.getBeanInfo(currentClass);
                    PropertyDescriptor propertyDescriptor = Arrays.stream(beanInfo.getPropertyDescriptors())
                                                                  .filter(pd -> pd.getName().equals(propertyPart))
                                                                  .findFirst()
                                                                  .orElseThrow(exceptionSupplier);
                    getterMethods.add(propertyDescriptor.getReadMethod());
                    currentClass = propertyDescriptor.getPropertyType();
                }
                type = currentClass;
            } catch (IntrospectionException e) {
                throw new RuntimeException(e);
            }
        }

        Object getValue(Object pojo) {
            Object result = pojo;
            for (Method method : getterMethods) {
                if (result == null) {
                    return null;
                }
                try {
                    result = method.invoke(result);
                } catch (ReflectiveOperationException e) {
                    throw new RuntimeException(e);
                }
            }
            return result;
        }

        Class<?> getType() {
            return type;
        }
    }
}
